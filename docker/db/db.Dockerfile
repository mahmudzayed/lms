ARG DB_VERSION=8.0.21
ARG DB_IMAGE=mysql

FROM ${DB_IMAGE}:${DB_VERSION}

#####################################
# Set Timezone
#####################################

ARG TZ=UTC
ENV TZ ${TZ}
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone \
    && chown -R mysql:root /var/lib/mysql/

COPY ./docker/db/my.cnf /etc/mysql/conf.d/my.cnf
COPY ./docker/db/docker-entrypoint-initdb.d /docker-entrypoint-initdb.d

CMD ["mysqld"]

EXPOSE 3306
