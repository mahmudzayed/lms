ARG ADMINER_VERSION=4.7.7
ARG ADMINER_IMAGE=adminer

FROM ${ADMINER_IMAGE}:${ADMINER_VERSION}

# Set User
USER adminer

# We expose Adminer on port 8080 (Adminer's default)
EXPOSE 8080
