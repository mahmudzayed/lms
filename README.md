# LMS

LMS for quick start-up.

## Installation

### Docker Based Installation

#### Step -01

- Git clone the app

#### Step -02

- Setup `docker/.env` file from `docker/.env.example`

    > Change the `WEB_HTTP_PUBLISH_PORT` and `WEB_HTTPS_PUBLISH_PORT` values if needed.
- Setup from `docker-compose.override.yml` file
  - In Development:
    - From `docker-compose.override.dev.yml`.
  - In Production:
    - From `docker-compose.override.prod.yml`.
        > Note: docker compose override files' priority sequence should be provided on .env file (from .env.example file)

- Setup `docker/.envs/app.env` file from `docker/.envs/app.env.example`
  > Note: Application env file is here, so any changes might require docker app service to restart
- Setup `docker/.envs/php-ini.env` file from `docker/.envs/php-ini.env.example`
  > Note: If required frequent updates it is recommended to set `PHP_INI_OPCACHE_ENABLE=0` in `php-ini.env` file
- Setup `docker/.envs/redis.env` file from `docker/.envs/redis.env.example`
- Setup `docker/.envs/web.env` file from `docker/.envs/web.env.example`

#### Step -03

- Goto `docker` directory. `cd docker/`
- Run `docker-compose build app` first inside for building app service
- Run `docker-compose build` for all services
- Create "lms-net" the external network manually using `docker network create lms-net`
- Run `docker-compose up -d` to bring up services
- Run `docker-compose ps -a` for checking process status

- Run `docker-compose exec app composer install` (use `--no-dev` flag in production)
- Run `docker-compose exec app php artisan migrate`
- Or, you can directly log into the bash with `docker-composer exec app bash` and run specific application configuration related command.
- Additional Notes
  > While using volume mouting in linux based operating systems, we might need to run the following commands for write permission:
  >
  > - Goto `codes` directory
  > - Run `sudo chown -R 1000:33 .`
  > - Run `sudo chmod -R ug+ws bootstrap storage` (for lumen)

### Manual Installation

- Download the app
- Setup `.env` file from `.env.example`
  - In Development:
    - Set `APP_DEBUG` to **true**.
    - Set `APP_ENV` to **local**.
  - In Production:
    - Set `APP_DEBUG` to **false**.
    - Set `APP_ENV` to **production**.
- Run `composer install` (use `--no-dev` flag in production)
- Run `php artisan key:generate`
  > (As it will not work in lumen, so we need to use the commented `/key` route of `routes/web.php` to generate the key.)
- Run `php artisan migrate`
- Run `php artisan db:seed`

#### Setting up Queue

**For shared hosting**

- Setup cronjob using the following setting:
  > `* * * * * php /var/www/artisan schedule:run >> /dev/null 2>&1`

**For cloud/dedicated hosting**

- Start background queue process:
  > `nohup php /var/www/artisan queue:work --tries=3 &`

#### Web Server

- **Development**
  - Run `php artisan serve` or
  - Use your Apache/nGinX/LAMP/XAMPP server configuration
- **Production**
  - Use your Apache/nGinX server configuration

## License

MIT License.
