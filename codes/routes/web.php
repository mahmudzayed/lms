<?php

use App\Http\Controllers as Base;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/issueBookSubmit', [Base\BooksIssuedController::class, 'index']);

Route::get('/booksList', [Base\BooksController::class, 'index']);
Route::get('/addBooks', [Base\BooksController::class, 'create']);
Route::post('/addBooks', [Base\BooksController::class, 'store']);

Route::get('/categoriesList', [Base\BooksCategoryController::class, 'index']);
Route::get('/addCategories', [Base\BooksCategoryController::class, 'create']);
Route::post('/addCategories', [Base\BooksCategoryController::class, 'store']);


Route::get('/issuedBooks', [Base\BooksCategoryController::class, 'index']);
Route::post('/issueBooks', [Base\BooksCategoryController::class, 'store']);
Route::get('/issueBooks', [Base\BooksCategoryController::class, 'create']);


Route::get('/membersList', [Base\MemberController::class, 'index']);
Route::get('/addMembers', [Base\MemberController::class, 'create']);
Route::post('/addMembers', [Base\MemberController::class, 'store']);

Route::get('/returnBooks', [Base\BooksReturnedController::class, 'create']);
Route::post('/returnBooks', [Base\BooksReturnedController::class, 'store']);

Route::get('/addThesis', [Base\ThesisController::class, 'create']);
Route::post('/addThesis', [Base\ThesisController::class, 'store']);

Route::get('/thesisList', function () {
    return view('thesisList');
});

Route::get('/main', function () {
    return view('layouts.sidebar');
});

// Auth::routes();

Route::get('/home', [Base\HomeController::class, 'index'])->name('home');


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';
