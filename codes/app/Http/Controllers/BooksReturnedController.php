<?php

namespace App\Http\Controllers;

use App\Models\BooksReturned;
use Illuminate\Http\Request;

class BooksReturnedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('returnBooks');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        BooksReturned::create([
            'bookId' => $req->bookId,
            'memId' => $req->memberId,
            'retDate' => $req->returnDate
        ]);

        return redirect('returnBooks');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BooksReturned  $booksReturned
     * @return \Illuminate\Http\Response
     */
    public function show(BooksReturned $booksReturned)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BooksReturned  $booksReturned
     * @return \Illuminate\Http\Response
     */
    public function edit(BooksReturned $booksReturned)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BooksReturned  $booksReturned
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BooksReturned $booksReturned)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BooksReturned  $booksReturned
     * @return \Illuminate\Http\Response
     */
    public function destroy(BooksReturned $booksReturned)
    {
        //
    }
}
