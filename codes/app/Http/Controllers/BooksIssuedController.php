<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BooksCategory;
use App\Models\BooksIssued;

class BooksIssuedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = BooksIssued::all();
        return view('issuedBooks', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('issueBooks');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $check = BooksIssued::create([
            'issueDate' => $req->issueDate,
            'retDate' => $req->returnDate,
            'bookId' => $req->bookId,
            'memId' => $req->memberId
        ]);

        return redirect('/issueBooks');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BooksCategory $booksCategory)
    {
        //
    }
}
