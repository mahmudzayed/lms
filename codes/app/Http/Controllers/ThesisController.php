<?php

namespace App\Http\Controllers;

use App\Models\BooksCategory;
use Illuminate\Http\Request;
use App\Models\Thesis;

class ThesisController extends Controller
{
    public function index()
    {
    }

    /**
     *store the newly created resources
     *@return null
     */

    public function store(Request $req)
    {

        $thesis = new Thesis();

        $thesis->thesisTitle = $req->title;
        $thesis->mem1ID = $req->mem1Id;
        $thesis->mem2ID = $req->mem2Id;
        $thesis->mem3ID = $req->mem3Id;
        $thesis->supName = $req->supName;

        $thesis->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addThesis');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BooksCategory $booksCategory)
    {
        //
    }
}
