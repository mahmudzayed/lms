<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Author;
use App\Models\BooksCategory;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        return view('booksList', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $auths = Author::all();
        $cats = BooksCategory::all();

        return view('addBooks', array('auths' => $auths, 'cats' => $cats));

        //return "Create Function called";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book();

        $book->bookTitle = $request->title;
        $book->edition = $request->edition;
        $book->authId = Author::where('name', $request->author)->first()->authId;
        $book->catId = BooksCategory::where('catName', '=', $request->cat)->first()->catId;
        $book->totalAvail = $request->booksAvail;

        $book->save();
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BooksCategory $booksCategory)
    {
        //
    }
}
