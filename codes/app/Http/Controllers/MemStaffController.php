<?php

namespace App\Http\Controllers;

use App\Models\MemStaff;
use Illuminate\Http\Request;

class MemStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MemStaff  $memStaff
     * @return \Illuminate\Http\Response
     */
    public function show(MemStaff $memStaff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MemStaff  $memStaff
     * @return \Illuminate\Http\Response
     */
    public function edit(MemStaff $memStaff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MemStaff  $memStaff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MemStaff $memStaff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MemStaff  $memStaff
     * @return \Illuminate\Http\Response
     */
    public function destroy(memStaff $memStaff)
    {
        //
    }
}
