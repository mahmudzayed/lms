<?php

namespace App\Http\Controllers;

use App\Models\BooksCategory;
use Illuminate\Http\Request;

use App\Models\Member;
use App\Models\MemStaff;
use App\Models\MemStudent;

class MemberController extends Controller
{

    //Listing the resources
    public function index()
    {
        $members = Member::all();
        return view('membersList', ['members' => $members]);
    }

    /**
     *
     *
     *
     *
     **/

    public function store(Request $req)
    {

        //Declaring member object
        $mem = new Member();
        $staff = new MemStaff();
        $student = new MemStudent();

        //Add data to members Table
        Member::create([
            'memName' => $req->name,
            'email' => $req->email,
            'contact' => $req->contact,
            'cnic' => $req->cnic,
            'dept' => $req->dept,
            'address' => $req->address,
            'memType' => $req->memType,
            'password' => $req->password
        ]);


        //Add data either to memStaff or memStudent depending on the value of memType field.
        if ($req->memType == "Student") {
            MemStudent::create([
                'memId' => Member::where('cnic', $req->cnic)->first()->memId,
                'regNo' => $req->regNo,
                'batch' => $req->batch,
            ]);
        } elseif ($req->memType == "Staff") {
            MemStaff::create([
                'memId' => Member::where('cnic', $req->cnic)->first()->memId,
                'designation' => $req->designation
            ]);
        }

        return redirect('/addMembers');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addMembers');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BooksCategory $booksCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BooksCategory  $booksCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BooksCategory $booksCategory)
    {
        //
    }
}
