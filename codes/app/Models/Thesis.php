<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thesis extends Model
{
    protected $table = 'thesis';
    public $timestamps = false;

    public function member()
    {
        return $this->belongsToMany('App\Models\Member');
    }
}
