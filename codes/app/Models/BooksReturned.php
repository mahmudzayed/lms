<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BooksReturned extends Model
{
    protected $primaryKey = 'returnId';
    protected $fillable = ['retDate', 'bookId', 'memId'];
    public $timestamps = false;

    public function member()
    {
        return $this->belongsTo('App\Models\Member', 'memId', 'memId');
    }
}
