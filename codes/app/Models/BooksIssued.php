<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BooksIssued extends Model
{
    protected $primaryKey = 'issueId';
    protected $fillable = ['issueDate', 'retDate', 'bookId', 'memId'];
    public $timestamps = false;

    public function member()
    {
        return $this->belongs('App\Models\Member', 'memId', 'memId');
    }
}
