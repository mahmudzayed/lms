<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{

    protected $fillable = ['memName', 'email', 'contact', 'cnic', 'dept', 'address', 'memType', 'password'];

    public function thesis()
    {
        return $this->belongsToMany('App\Models\Thesis');
    }

    public function memStaff()
    {
        return $this->belongsTo('App\Models\MemStaff');
    }

    public function memStudent()
    {
        return $this->belongsTo('App\Models\MemStudent');
    }

    public function issuedBooks()
    {
        return $this->hasMany('App\Models\BooksIssued');
    }

    public function returnBooks()
    {
        return $this->hasMany('App\Models\BooksReturned');
    }
}
