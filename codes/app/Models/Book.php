<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $primarykey = 'bookId';

    public function category()
    {
        return $this->belongsTo('App\Models\BooksCategory', 'catId', 'catId');
    }

    public function author()
    {
        return $this->belongsTo('authors');
    }
}
