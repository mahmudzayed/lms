<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemStudent extends Model
{
    public $timestamps = false;
    protected $fillable = ['memId', 'regNo', 'batch'];
    public function member()
    {
        return $this->hasMany('App\Models\Member');
    }
}
