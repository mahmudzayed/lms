<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemStaff extends Model
{

    public $timestamps = false;
    protected $fillable = ['memId', 'designation'];

    public function member()
    {
        return $this->hasMany('App\Models\Member');
    }
}
